#!/bin/bash

run='run3'
	
# mkdir $run

if false; then
mkdir $run/preprocess


# Preprocess - Merge the R1 and R2, detect 5' primer, detect 3' primer, without N
preprocess.py illumina \
--merge-software pear \
--min-amplicon-size 400 --max-amplicon-size 500 \
--five-prim-primer ACGGRAGGCAGCAG \
--three-prim-primer  AGGATTAGATACCCTGGTA \
--R1-size 250 --R2-size 250 \
--mismatch-rate 0.1 \
--nb-cpus 8 \
--input-archive ../archive.tar \
--output-dereplicated $run/preprocess/dereplication.fasta \
--output-count $run/preprocess/count.tsv \
--summary $run/preprocess/summary.html \
--log-file $run/preprocess/log_file.log

fi

if false; then
mkdir $run/cluster

# Clustering , Distance 1 then Distance 3
clustering.py \
--nb-cpus 16 --distance 3 \
--input-fasta  $run/preprocess/dereplication.fasta \
--input-count $run/preprocess/count.tsv \
--output-biom $run/cluster/swarms_abundance.biom \
--output-fasta $run/cluster/seeds.fasta \
--output-compo $run/cluster/swarms_composition.tsv \
--log-file $run/cluster/log_file.log \
--denoising

# Cluster Stat, braycurtis and average
clusters_stat.py \
--distance-method braycurtis \
--linkage-method average \
--input-biom $run/cluster/swarms_abundance.biom \
--output-file $run/cluster/clusters_metrics.html \
--log-file $run/cluster/log_file.log

fi

if false; then
mkdir $run/chimera

# Remove Chimera
remove_chimera.py \
--nb-cpus 16 \
--input-fasta $run/cluster/seeds.fasta \
--input-biom $run/cluster/swarms_abundance.biom \
--non-chimera $run/chimera/non_chimera.fasta \
--summary $run/chimera/summary.html \
--out-abundance $run/chimera/non_chimera_abundance.biom \
--log-file $run/chimera/log_file.log

fi

if false; then
mkdir $run/filters

# Filters Min Abundance 0.00005
filters.py \
--nb-cpus 16 \
--input-biom $run/chimera/non_chimera_abundance.biom \
--input-fasta $run/chimera/non_chimera.fasta \
--output-fasta $run/filters/filtered.fasta \
--output-biom $run/filters/filtered.biom \
--excluded $run/filters/excluded.tsv \
--summary $run/filters/summary.html \
--log-file $run/filters/log_file.log \
--min-abundance 0.00005

fi


if false; then
mkdir $run/affiliation

# Affiliation OTU silva_132_16S_pintail100.fasta
affiliation_OTU.py \
--reference /db/frogs_databanks/assignation/16S/silva_132_16S_pintail50/silva_132_16S_pintail50.fasta \
--input-biom $run/filters/filtered.biom \
--input-fasta $run/filters/filtered.fasta \
--output-biom $run/affiliation/affiliation.biom \
--summary $run/affiliation/summary.html \
--log-file $run/affiliation/log_file.log \
--nb-cpus 16 --java-mem 20 --rdp

# Affiliation Stat
affiliations_stat.py \
--input-biom $run/affiliation/affiliation.biom \
--output-file $run/affiliation/affiliations_metrics.html \
--log-file $run/affiliation/log_file.log \
--rarefaction-ranks Domain Phylum Class Order Family Genus Species \
--taxonomic-ranks Domain Phylum Class Order Family Genus Species \
--multiple-tag blast_affiliations \
--tax-consensus-tag blast_taxonomy \
--identity-tag perc_identity \
--coverage-tag perc_query_coverage

# Affiliation Stat RDP
affiliations_stat.py \
--input-biom $run/affiliation/affiliation.biom \
--output-file $run/affiliation/affiliations_metrics_rdp.html \
--log-file $run/affiliation/log_file_rdp.log \
--rarefaction-ranks Domain Phylum Class Order Family Genus Species \
--taxonomic-ranks Domain Phylum Class Order Family Genus Species \
--taxonomy-tag rdp_taxonomy \
--bootstrap-tag rdp_bootstrap

fi

if false; then
mkdir $run/biom_to_csv

# Creation abundance + multi aff TSV
biom_to_tsv.py \
--input-biom $run/affiliation/affiliation.biom \
--input-fasta $run/filters/filtered.fasta \
--output-tsv $run/biom_to_csv/abundance.tsv \
--output-multi-affi $run/biom_to_csv/multi_affiliation.tsv \
--log-file $run/biom_to_csv/log_file.log

cp -r $run/biom_to_csv $run/tsv_corrected

fi


if false; then
mkdir $run/tsv_to_biom
tsv_to_biom.py \
--input-tsv $run/tsv_corrected/abundance.tsv \
--input-multi-affi $run/tsv_corrected/multi_affiliation.tsv \
--output-biom $run/tsv_to_biom/affiliation_corrected.biom \
--output-fasta $run/tsv_to_biom/fasta_corrected.fasta \
--log-file $run/tsv_to_biom/log_file.log

# Affiliation Stat
affiliations_stat.py \
--input-biom $run/tsv_to_biom/affiliation_corrected.biom \
--output-file $run/tsv_to_biom/affiliations_metrics.html \
--log-file $run/tsv_to_biom/log_file_aff.log \
--rarefaction-ranks Domain Phylum Class Order Family Genus Species \
--taxonomic-ranks Domain Phylum Class Order Family Genus Species \
--multiple-tag blast_affiliations \
--tax-consensus-tag blast_taxonomy \
--identity-tag perc_identity \
--coverage-tag perc_query_coverage

fi

if false; then
mkdir $run/tree

# Frogs Tree
tree.py \
--input-otu $run/tsv_to_biom/fasta_corrected.fasta \
--biomfile $run/tsv_to_biom/affiliation_corrected.biom \
--nb-cpus 10 \
--out-tree $run/tree/tree.nwk \
--html $run/tree/summary.html \
--log-file $run/tree/log_file.log

fi

if false; then
mkdir $run/std_Biom

biom_to_stdBiom.py \
--input-biom $run/tsv_to_biom/affiliation_corrected.biom \
--output-biom $run/std_Biom/abundance.biom \
--output-metadata $run/std_Biom/blast_metadata.tsv \
--log-file $run/std_Biom/log_file.log 


fi









##    Taxonomie corrected


if true; then
#mkdir $run/tsv_to_biom
tsv_to_biom.py \
--input-tsv $run/tsv_corrected/abundance_taxonomie_corrected.tsv \
--input-multi-affi $run/tsv_corrected/multi_affiliation_taxonomie_corrected.tsv \
--output-biom $run/tsv_to_biom/affiliation_taxonomie_corrected.biom \
--output-fasta $run/tsv_to_biom/fasta_taxonomie_corrected.fasta \
--log-file $run/tsv_to_biom/log_file_taxonomie_corrected.log
fi

if true; then
# Affiliation Stat
affiliations_stat.py \
--input-biom $run/tsv_to_biom/affiliation_taxonomie_corrected.biom \
--output-file $run/tsv_to_biom/affiliations_metrics_taxonomie_corrected.html \
--log-file $run/tsv_to_biom/log_file_aff_taxonomie_corrected.log \
--rarefaction-ranks Domain Phylum Class Order Family Genus Species \
--taxonomic-ranks Domain Phylum Class Order Family Genus Species \
--multiple-tag blast_affiliations \
--tax-consensus-tag blast_taxonomy \
--identity-tag perc_identity \
--coverage-tag perc_query_coverage

fi

if true; then
#mkdir $run/tree

# Frogs Tree
tree.py \
--input-otu $run/tsv_to_biom/fasta_taxonomie_corrected.fasta \
--biomfile $run/tsv_to_biom/affiliation_taxonomie_corrected.biom \
--nb-cpus 10 \
--out-tree $run/tree/tree_taxonomie_corrected.nwk \
--html $run/tree/summary_taxonomie_corrected.html \
--log-file $run/tree/log_file_taxonomie_corrected.log

fi

if true; then
#mkdir $run/std_Biom

biom_to_stdBiom.py \
--input-biom $run/tsv_to_biom/affiliation_taxonomie_corrected.biom \
--output-biom $run/std_Biom/abundance_taxonomie_corrected.biom \
--output-metadata $run/std_Biom/blast_metadata_taxonomie_corrected.tsv \
--log-file $run/std_Biom/log_file_taxonomie_corrected.log 


fi


